# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

from unittest.mock import MagicMock, Mock, call, mock_open, patch

import io
import pathlib

import pytest

from hazard.hazard import Hazard, auto


def test_init():
    instance = Hazard()
    assert not instance._gitlab_data
    assert not instance._git_data


def test_fetch():
    directory = pathlib.Path("foo")

    gitlab_data = Mock()
    gitlab_data.return_value.get_source_ref.return_value = "source_ref"
    gitlab_data.return_value.get_target_ref.return_value = "target_ref"

    git_data = Mock()

    instance = Hazard(_directory=directory)
    instance.fetch(_gitlab_data=gitlab_data, _git_data=git_data)

    gitlab_data.assert_called_with()
    gitlab_data.return_value.fetch.assert_called_with()
    git_data.assert_called_with(directory)
    git_data.return_value.fetch.assert_called_with("source_ref", "target_ref")

    assert instance._gitlab_data
    assert instance._git_data


def test_evaluate_open():
    directory = pathlib.Path("foo")

    with patch("builtins.open", mock_open(read_data="")) as dangerfile:
        instance = Hazard(_directory=directory)
        instance.evaluate()
        dangerfile.assert_called_with(directory / "dangerfile.py", encoding="utf-8")
        dangerfile.return_value.read.assert_called_with()


@pytest.mark.parametrize(
    "expected_exit_code,dangerfile_contents",
    [
        (1, 'fail(""); warn(""); message("");'),
        (1, 'fail(""); message("");'),
        (0, 'warn(""); message("");'),
        (0, 'message("");'),
        (0, ""),
    ],
    ids=lambda a: a.replace('("");', "") if isinstance(a, str) else None,
)
def test_evaluate_result(dangerfile_contents, expected_exit_code):
    dangerfile = Mock()
    dangerfile.read.return_value = dangerfile_contents
    dangerfile_cm = MagicMock(__enter__=Mock(return_value=dangerfile))

    instance = Hazard()
    assert instance.evaluate(_dangerfile=dangerfile_cm) == expected_exit_code
    dangerfile.read.assert_called_with()


@pytest.mark.parametrize("category", ["message", "warn", "fail"])
def test_feedback_callbacks(category):
    dangerfile = io.StringIO(f"{category}('First text'); {category}('Second text')")
    mock_cb = Mock()
    kwargs = {f"_{category}_cb": mock_cb}
    instance = Hazard()
    instance.evaluate(_dangerfile=dangerfile, **kwargs)
    assert mock_cb.mock_calls == [call("First text"), call("Second text")]


@pytest.mark.parametrize("category", ["message", "warn", "fail"])
def test_feedback(category):
    dangerfile = io.StringIO(f"{category}('First text'); {category}('Second text')")
    instance = Hazard()
    instance.evaluate(_dangerfile=dangerfile)
    assert getattr(instance, f"{category}s") == ["First text", "Second text"]


DANGERFILE_PY = """
message("This is a message.")
warn("This is a warning.")
fail("This is a failure.")

message(danger.gitlab.mr.description)
warn(danger.gitlab.mr.target_branch)
fail(danger.gitlab.mr.title)

message(danger.git.commits[0].message)
warn(danger.git.created_files[0])
fail(danger.git.modified_files[0])
"""


def test_feedback_integration():
    instance = Hazard()

    gitlab_data = Mock()
    gitlab_data.return_value.mr.description = "Closes #1"
    gitlab_data.return_value.mr.target_branch = "main"
    gitlab_data.return_value.mr.title = "Add a feature"

    git_data = Mock()
    git_data.return_value.commits = [Mock(message="Apply a change")]
    git_data.return_value.created_files = ["foo.txt"]
    git_data.return_value.modified_files = ["bar.txt"]

    instance.fetch(_gitlab_data=gitlab_data, _git_data=git_data)
    instance.evaluate(_dangerfile=io.StringIO(DANGERFILE_PY))

    assert instance.messages == ["This is a message.", "Closes #1", "Apply a change"]
    assert instance.warns == ["This is a warning.", "main", "foo.txt"]
    assert instance.fails == ["This is a failure.", "Add a feature", "bar.txt"]


def test_prepare_comment_empty():
    generator = Mock()

    instance = Hazard()
    instance.fetch(_gitlab_data=Mock(), _git_data=Mock())
    instance._prepare_comment(_generator=generator)

    assert not generator.mock_calls


PREPARE_COMMENT_TESTS = {
    "3_tables": [
        ("messages", "Messages", ["This is a message.", "This is another message."]),
        ("warns", "Warnings", ["This is a warning.", "This is another warning."]),
        ("fails", "Failures", ["This is a failure.", "This is another failure."]),
    ],
    "2_tables": [
        ("fails", "Failures", ["This is a different failure."]),
        ("messages", "Messages", ["This is a different message."]),
    ],
    "1_table": [
        ("warns", "Warnings", ["This is a different warning."]),
    ],
}


@pytest.mark.parametrize("case_name", PREPARE_COMMENT_TESTS.keys())
def test_prepare_comment(case_name):
    generator = Mock()

    instance = Hazard()
    instance.fetch(
        _gitlab_data=Mock(),
        _git_data=Mock(
            return_value=Mock(head="0123456789abcdef0123456789abcdef01234567")
        ),
    )
    for category, _, entries in PREPARE_COMMENT_TESTS[case_name]:
        setattr(instance, category, entries)
    instance._prepare_comment(_generator=generator)

    generator.add_comment.assert_called_with("DangerID: danger-id-Danger;")
    for _, title, entries in PREPARE_COMMENT_TESTS[case_name]:
        assert call(title, entries) in generator.add_table.mock_calls
    generator.add_footer.assert_called_with("0123456789abcdef0123456789abcdef01234567")
    generator.generate.assert_called_with()


def test_comment():
    gitlab = Mock()
    instance = Hazard()
    instance.fetch(_gitlab_data=gitlab, _git_data=Mock())
    instance.comment(_body="<p>This is a comment.</p>")
    gitlab.return_value.sync_comment.assert_called_with("<p>This is a comment.</p>")


def test_report():
    stdout = io.StringIO()

    instance = Hazard(_stdout=stdout)
    instance.fails = ["Failure #1", "Failure #2"]
    instance.warns = ["Warning #1"]
    instance.messages = []

    expected_report_lines = ["FAILURES: 2", "WARNINGS: 1", "MESSAGES: 0", ""]

    instance.report()

    assert stdout.getvalue() == "\n".join(expected_report_lines)


def test_auto():
    directory = pathlib.Path("foo")
    stdout = io.StringIO()

    hazard = Mock()
    hazard.return_value.evaluate.return_value = 42

    assert auto(_directory=directory, _stdout=stdout, _hazard=hazard) == 42

    hazard.assert_called_with(_directory=directory, _stdout=stdout)
    hazard.return_value.fetch.assert_called_with()
    hazard.return_value.evaluate.assert_called_with()
    hazard.return_value.comment.assert_called_with()
    hazard.return_value.report.assert_called_with()
