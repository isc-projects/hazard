# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

import pathlib

import pygit2
import pytest

from unittest.mock import patch

from hazard.git import HazardGitData


@pytest.fixture
def repository(tmp_path):
    def commit_file_contents(repo, ref, file, contents, message, parent=None):
        if parent:
            parents = [parent]
        else:
            try:
                parents = [repo.references[ref].target]
            except KeyError:
                parents = []

        if parents:
            repo.index.read_tree(repo[parents[0]].tree)

        file_path = pathlib.Path(repo.path).parent / file
        if contents:
            file_path.write_text(contents)
            repo.index.add(file)
        else:
            file_path.unlink()
            repo.index.remove(file)

        repo.index.write()
        tree = repo.index.write_tree()

        author = pygit2.Signature("Jane Doe", "localhost@localhost")
        return repo.create_commit(ref, author, author, message, tree, parents)

    repo = pygit2.init_repository(tmp_path)
    repo.remotes.create("origin", f"file://{tmp_path}")

    main_ref = "refs/heads/main"
    commit_file_contents(repo, main_ref, "foo.txt", "Hello!", "Add foo.txt")
    base = commit_file_contents(repo, main_ref, "bar.txt", "Welcome!", "Add bar.txt")
    commit_file_contents(repo, main_ref, "baz.txt", "Greetings!", "Add baz.txt")

    topic_ref = "refs/merge-requests/1337/head"
    commit_file_contents(
        repo, topic_ref, "foo.txt", "Goodbye!", "Update foo.txt", parent=base
    )
    commit_file_contents(repo, topic_ref, "bar.txt", None, "Delete bar.txt")
    commit_file_contents(repo, topic_ref, "baq.txt", "Hi!", "Add baq.txt")

    return repo


def test_init(repository):
    instance = HazardGitData(pathlib.Path(repository.path))
    assert instance._repo
    assert not instance.commits
    assert not instance.created_files
    assert not instance.modified_files
    assert not instance.deleted_files


def test_fetch(repository):
    instance = HazardGitData(pathlib.Path(repository.path))
    instance.fetch("refs/merge-requests/1337/head", "refs/heads/main")
    assert instance._repo.references[instance.SOURCE_REF]
    assert instance._repo.references[instance.TARGET_REF]


def test_fetch_bad_source_ref(repository):
    bad_source_ref = "refs/merge-requests/42/head"
    instance = HazardGitData(pathlib.Path(repository.path))
    with pytest.raises(HazardGitData.FetchException, match=bad_source_ref):
        instance.fetch(bad_source_ref, "refs/heads/main")


def test_fetch_bad_target_ref(repository):
    bad_target_ref = "refs/heads/foo"
    instance = HazardGitData(pathlib.Path(repository.path))
    with pytest.raises(HazardGitData.FetchException, match=bad_target_ref):
        instance.fetch("refs/merge-requests/1337/head", bad_target_ref)


def test_fetch_depth(repository):
    env = {"HAZARD_FETCH_DEPTH": 42}
    with patch("pygit2.Remote.fetch") as fetch:
        with pytest.raises(HazardGitData.FetchException):
            instance = HazardGitData(pathlib.Path(repository.path), env)
            instance.fetch("foo", "bar")
        assert fetch.call_args.kwargs == {"depth": 42}


def test_commits(repository):
    instance = HazardGitData(pathlib.Path(repository.path))
    instance.fetch("refs/merge-requests/1337/head", "refs/heads/main")
    source_head = repository.references[instance.SOURCE_REF].target
    source_head_parent = repository[source_head].parents[0].id
    target_head = repository.references[instance.TARGET_REF].target
    target_head_parent = repository[target_head].parents[0].id
    assert len(instance.commits) == 3
    assert len(instance.commits[0].sha) == 40
    assert len(instance.commits[1].sha) == 40
    assert len(instance.commits[2].sha) == 40
    assert instance.commits[0].sha == str(source_head)
    assert instance.commits[1].sha == str(source_head_parent)
    assert instance.commits[0].author.name == "Jane Doe"
    assert instance.commits[1].author.email == "localhost@localhost"
    assert instance.commits[0].parents[0].id == source_head_parent
    assert instance.commits[2].parents[0].id == target_head_parent


def test_head(repository):
    instance = HazardGitData(pathlib.Path(repository.path))
    instance.fetch("refs/merge-requests/1337/head", "refs/heads/main")
    assert len(instance.head) == 40
    assert instance.head == instance.commits[0].sha


def test_created_files(repository):
    instance = HazardGitData(pathlib.Path(repository.path))
    instance.fetch("refs/merge-requests/1337/head", "refs/heads/main")
    assert instance.created_files == ["baq.txt"]


def test_modified_files(repository):
    instance = HazardGitData(pathlib.Path(repository.path))
    instance.fetch("refs/merge-requests/1337/head", "refs/heads/main")
    assert instance.modified_files == ["foo.txt"]


def test_deleted_files(repository):
    instance = HazardGitData(pathlib.Path(repository.path))
    instance.fetch("refs/merge-requests/1337/head", "refs/heads/main")
    assert instance.deleted_files == ["bar.txt"]
