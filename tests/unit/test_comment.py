# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

from hazard.comment import HazardComment


def test_init():
    instance = HazardComment()
    assert instance


def test_generate():
    instance = HazardComment()
    assert instance.generate() == ""


def test_add_comment():
    instance = HazardComment()
    instance.add_comment("foo")
    instance.add_comment("foo1")
    result = instance.generate()

    assert "<!-- foo -->" in result
    assert "<!-- foo1 -->" in result


def test_add_table():
    instance = HazardComment()
    instance.add_table("Foo", ["*bar*", "**baz**"])
    instance.add_table("Baq", ["`bax`"])
    result = instance.generate()

    assert '<table name="foo">' in result
    assert '<th width="100%">Foo</th>' in result
    assert "<td><em>bar</em></td>" in result
    assert "<td><strong>baz</strong></td>" in result
    assert '<table name="baq">' in result
    assert '<th width="100%">Baq</th>' in result
    assert "<td><code>bax</code></td>" in result


def test_add_footer():
    instance = HazardComment()
    instance.add_footer("foo")
    result = instance.generate()

    assert "foo" in result
