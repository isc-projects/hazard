# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

from dataclasses import dataclass
from unittest.mock import Mock

from hazard.gitlab import HazardGitLabData


def test_init():
    env = {
        "CI_SERVER_URL": "https://localhost/",
        "CI_MERGE_REQUEST_PROJECT_ID": "1337",
        "CI_MERGE_REQUEST_IID": "42",
        "DANGER_GITLAB_API_TOKEN": "glpat-0123456789abcdefghij",
    }
    instance = HazardGitLabData(_env=env)
    assert instance._server_url == "https://localhost/"
    assert instance._project_id == 1337
    assert instance._merge_request_id == 42
    assert instance._access_token == "glpat-0123456789abcdefghij"


def test_fetch():
    env = {
        "CI_SERVER_URL": "https://localhost/",
        "CI_MERGE_REQUEST_PROJECT_ID": "1337",
        "CI_MERGE_REQUEST_IID": "42",
    }
    gitlab = Mock()
    instance = HazardGitLabData(_env=env, _gitlab=gitlab)
    instance.fetch()
    assert instance.mr

    gitlab_calls = str(gitlab.mock_calls)
    assert "https://localhost/" in gitlab_calls
    assert "1337" in gitlab_calls
    assert "42" in gitlab_calls


def test_refs():
    merge_request = Mock()
    merge_request.iid = 1337
    merge_request.target_branch = "main"

    instance = HazardGitLabData()
    source_ref = instance.get_source_ref(_merge_request=merge_request)
    target_ref = instance.get_target_ref(_merge_request=merge_request)

    assert source_ref == "refs/merge-requests/1337/head"
    assert target_ref == "refs/heads/main"


@dataclass
class MockComment:
    id: int
    body: str


def test_find_comment_positive():
    merge_request = Mock()
    merge_request.notes.list.return_value = [
        MockComment(id=1337, body="This is a comment."),
        MockComment(id=42, body="<!-- DangerID: danger-id-Danger; -->"),
    ]

    instance = HazardGitLabData()
    assert instance._find_comment(_merge_request=merge_request).id == 42


def test_find_comment_negative():
    merge_request = Mock()
    merge_request.notes.list.return_value = [
        MockComment(id=1337, body="This is a comment."),
        MockComment(id=42, body="<!-- This is another comment. -->"),
    ]

    instance = HazardGitLabData()
    assert not instance._find_comment(_merge_request=merge_request)


def test_sync_comment_noop():
    merge_request = Mock()

    instance = HazardGitLabData()
    instance.sync_comment(
        "",
        _merge_request=merge_request,
        _comment_finder=Mock(return_value=None),
    )

    assert not merge_request.notes.create.called
    assert not merge_request.notes.update.called
    assert not merge_request.notes.delete.called


def test_sync_comment_create():
    merge_request = Mock()

    instance = HazardGitLabData()
    instance.sync_comment(
        "Comment #1",
        _merge_request=merge_request,
        _comment_finder=Mock(return_value=None),
    )

    merge_request.notes.create.assert_called_with({"body": "Comment #1"})
    assert not merge_request.notes.update.called
    assert not merge_request.notes.delete.called


def test_sync_comment_replace():
    merge_request = Mock()

    instance = HazardGitLabData()
    instance.sync_comment(
        "Comment #2",
        _merge_request=merge_request,
        _comment_finder=Mock(return_value=Mock(id=1337)),
    )

    assert not merge_request.notes.create.called
    merge_request.notes.update.assert_called_with(1337, {"body": "Comment #2"})
    assert not merge_request.notes.delete.called


def test_sync_comment_delete():
    merge_request = Mock()

    instance = HazardGitLabData()
    instance.sync_comment(
        "",
        _merge_request=merge_request,
        _comment_finder=Mock(return_value=Mock(id=1337)),
    )

    assert not merge_request.notes.create.called
    assert not merge_request.notes.update.called
    merge_request.notes.delete.assert_called_with(1337)
