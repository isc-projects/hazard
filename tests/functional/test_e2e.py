# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

from dataclasses import dataclass
from typing import Dict, List, Optional

import collections
import contextlib
import html.parser
import http
import http.server
import io
import json
import multiprocessing
import os
import pathlib
import queue
import selectors
import shutil
import subprocess

import pytest

import hazard


@dataclass
class E2ETestCase:
    expected_exit_code: int
    expected_report: str
    prohibited_request: Optional[str] = None
    required_request: Optional[str] = None
    required_table_cells: Optional[Dict[str, List[str]]] = None


E2E_TESTS = {
    "01_comment_noop": E2ETestCase(
        expected_exit_code=0,
        expected_report="FAILURES: 0\nWARNINGS: 0\nMESSAGES: 0\n",
        prohibited_request="POST /api/v4/projects/1/merge_requests/8514/notes",
    ),
    "02_comment_create": E2ETestCase(
        expected_exit_code=1,
        expected_report="FAILURES: 3\nWARNINGS: 3\nMESSAGES: 3\n",
        required_request="POST /api/v4/projects/1/merge_requests/8514/notes",
        required_table_cells={
            "messages": [
                "This is a <em>message</em>.",
                "Remove support for legacy system test runner.",
                "Add baq.txt",
            ],
            "warnings": [
                "This is a <strong>warning</strong>.",
                "The target branch for this MR is 'main'",
                "baq.txt",
            ],
            "failures": [
                "This is a <code>failure</code>.",
                "Remove legacy system test runner",
                "foo.txt",
            ],
        },
    ),
    "03_comment_update_two_tables": E2ETestCase(
        expected_exit_code=1,
        expected_report="FAILURES: 3\nWARNINGS: 0\nMESSAGES: 3\n",
        required_request="PUT /api/v4/projects/1/merge_requests/8514/notes/419764",
        required_table_cells={
            "messages": [
                "This is a different <em>message</em>.",
                "Merge request !8514",
                "Delete bar.txt",
            ],
            "failures": [
                "This is a different <code>failure</code>.",
                "Status: can_be_merged",
                "bar.txt",
            ],
        },
    ),
    "04_comment_update_one_table": E2ETestCase(
        expected_exit_code=0,
        expected_report="FAILURES: 0\nWARNINGS: 0\nMESSAGES: 3\n",
        required_request="PUT /api/v4/projects/1/merge_requests/8514/notes/419764",
        required_table_cells={
            "messages": [
                "This is a different <em>message</em> still.",
                "Affects v9.19, Review, Tests, v9.19",
                "40, 40",
            ]
        },
    ),
    "05_comment_delete": E2ETestCase(
        expected_exit_code=0,
        expected_report="FAILURES: 0\nWARNINGS: 0\nMESSAGES: 0\n",
        required_request="DELETE /api/v4/projects/1/merge_requests/8514/notes/419764",
    ),
}


@pytest.mark.parametrize("case_name", E2E_TESTS.keys())
@pytest.mark.usefixtures("testcase_files", "ci_environment")
def test_e2e(tmp_path, case_name, git_repository):
    """
    Set up a Git repository, environment variables, and an HTTP server
    imitating GitLab.  Execute the package entry point.  This should cause the
    dangerfile.py present in a given test case's subdirectory to be evaluated,
    which in turn should result in GitLab comments being posted, updated, and
    removed as appropriate, along with the exit code being set accordingly.
    """
    case = E2E_TESTS[case_name]

    requests = {}
    stdout = io.StringIO()
    with gitlab_imitation(tmp_path) as request_log:
        exit_code = hazard.hazard.auto(_directory=tmp_path, _stdout=stdout)
        assert exit_code == case.expected_exit_code
        report = stdout.getvalue()
        assert report == case.expected_report
        while True:
            try:
                path, body = request_log.get(block=False)
            except queue.Empty:
                break
            requests[path] = body

    if case.prohibited_request:
        assert case.prohibited_request not in requests.keys()
        return

    assert case.required_request
    assert case.required_request in requests.keys()

    if not case.required_table_cells:
        return

    request_json = requests[case.required_request]
    request = json.loads(request_json) if request_json else {}

    parser = CommentHTMLParser()
    parser.feed(request.get("body", ""))

    for table, cells in case.required_table_cells.items():
        assert table in parser.tables.keys()
        for cell in cells:
            assert cell in parser.tables[table]

    assert [c for c in parser.comments if "DangerID: danger-id-Danger;" in c]
    assert [p for p in parser.paragraphs if git_repository["source_head"] in p]

    # Failures should always be listed before warnings and messages.  Warnings
    # should always be listed before messages.  However, not all three tables
    # necessarily have to exist in every GitLab comment.
    table_order = []
    for table in ("failures", "warnings", "messages"):
        if table in parser.tables:
            table_order.append(list(parser.tables.keys()).index(table))
    assert table_order == sorted(table_order)


@pytest.fixture
def testcase_files(tmp_path, case_name):
    test_dir = pathlib.Path(__file__).with_suffix("")
    common_dir = test_dir / "common"
    case_dir = test_dir / case_name

    shutil.copytree(common_dir, tmp_path, dirs_exist_ok=True)
    shutil.copytree(case_dir, tmp_path, dirs_exist_ok=True)


@pytest.fixture
def git_repository(tmp_path):
    def run(binary, args):
        cmd = f"{binary} -C {tmp_path} {args}"
        output = subprocess.check_output(cmd, shell=True)
        return output.decode("utf-8").strip()

    git = shutil.which("git")
    assert git, "This test requires the 'git' binary to be present in PATH"

    run(git, "init")
    run(git, f"remote add origin file://{tmp_path}")
    run(git, "config --local user.name 'Jane Doe'")
    run(git, "config --local user.email 'localhost@localhost'")

    run(git, "branch -m main")

    pathlib.Path(tmp_path / "foo.txt").write_text("Hello!")
    run(git, "add foo.txt")
    run(git, "commit -m 'Add foo.txt'")

    pathlib.Path(tmp_path / "bar.txt").write_text("Welcome!")
    run(git, "add bar.txt")
    run(git, "commit -m 'Add bar.txt'")

    pathlib.Path(tmp_path / "baz.txt").write_text("Greetings!")
    run(git, "add baz.txt")
    run(git, "commit -m 'Add baz.txt'")

    run(git, "checkout -b merge-request HEAD^")

    pathlib.Path(tmp_path / "foo.txt").write_text("Goodbye!")
    run(git, "commit -m 'Update foo.txt' foo.txt")

    run(git, "rm bar.txt")
    run(git, "commit -m 'Delete bar.txt'")

    pathlib.Path(tmp_path / "baq.txt").write_text("Hi!")
    run(git, "add baq.txt")
    run(git, "commit -m 'Add baq.txt'")

    run(git, "update-ref refs/merge-requests/8514/head HEAD")

    yield {"source_head": run(git, "rev-list -n1 HEAD")}


@pytest.fixture
def ci_environment():
    os.environ["CI_SERVER_URL"] = "http://localhost:8000/"
    os.environ["CI_MERGE_REQUEST_PROJECT_ID"] = "1"
    os.environ["CI_MERGE_REQUEST_IID"] = "8514"
    os.environ["DANGER_GITLAB_API_TOKEN"] = "glpat-0123456789abcdefghij"


@contextlib.contextmanager
def gitlab_imitation(root_dir):
    def start_gitlab_process(root_dir, request_log, control_pipe):
        listen_on = ("localhost", 8000)
        httpd = GitLabServer(
            root_dir, request_log, control_pipe, listen_on, GitLabImitation
        )
        control_pipe.send("ready")
        httpd.serve_until_done()

    request_log = multiprocessing.Queue()
    control_parent, control_child = multiprocessing.Pipe()

    gitlab = multiprocessing.Process(
        target=start_gitlab_process, args=(root_dir, request_log, control_child)
    )
    gitlab.start()

    assert control_parent.recv() == "ready"
    try:
        yield request_log
    finally:
        control_parent.send("done")
        gitlab.join()

    assert gitlab.exitcode == 0


class GitLabServer(http.server.HTTPServer):
    """
    An HTTP server imitating GitLab, running as a separate process.  It has to
    exit cleanly in order for code coverage data to be collected properly,
    which is why a Pipe object shared with the parent process is used instead
    of HTTPServer.serve_forever() + Process.terminate(), even though the latter
    combination would have been a much simpler solution.
    """

    def __init__(
        self, root_dir, request_log, control_pipe, server_address, RequestHandlerClass
    ):
        super().__init__(server_address, RequestHandlerClass)
        self.root_dir = root_dir
        self.request_log = request_log
        self._control_pipe = control_pipe

    def serve_until_done(self):
        selector = selectors.DefaultSelector()
        selector.register(self, selectors.EVENT_READ)
        selector.register(self._control_pipe, selectors.EVENT_READ)

        while True:
            events = selector.select()
            for fileobj in [e[0].fileobj for e in events]:
                if fileobj == self:
                    self.handle_request()
                elif fileobj == self._control_pipe:
                    assert self._control_pipe.recv() == "done"
                    return


class GitLabImitation(http.server.BaseHTTPRequestHandler):
    """
    An HTTP request handler imitating a GitLab instance and logging the
    requests sent to it.  The responses the handler sends are read from JSON
    files on disk; for example, the response to:

        GET /api/v4/projects/1/merge_requests/8514/notes?sort=asc

    is read from a file called:

        GET-api-v4-projects-1-merge_requests-8514-notes-sort-asc.json

    If a file corresponding to a given request does not exist on disk (in the
    directory specified as the root directory for a given server instance, see
    the 'root_dir' parameter of GitLabServer.__init__()), a FileNotFoundError
    exception is raised.

    All requests are expected to be authorized by a specific, fixed access
    token ("glpat-0123456789abcdefghij").
    """

    def do_GET(self):
        self.respond()

    def do_POST(self):
        self.respond()

    def do_PUT(self):
        self.respond()

    def do_DELETE(self):
        self.respond()

    def respond(self):
        assert self.headers["PRIVATE-TOKEN"] == "glpat-0123456789abcdefghij"

        request = f"{self.command} {self.path}"
        body = self.read_request_body()
        self.server.request_log.put((request, body))

        normalized_url = self.path
        for char in "/?=&":
            normalized_url = normalized_url.replace(char, "-")
        response_file_name = self.command + normalized_url + ".json"
        response_headers_file_name = response_file_name + ".headers.txt"

        with open(self.server.root_dir / response_file_name, "rb") as response_file:
            response = response_file.read()

        headers = {}
        try:
            with open(
                self.server.root_dir / response_headers_file_name, encoding="utf-8"
            ) as response_headers_file:
                for line in response_headers_file:
                    header, value = line.split(": ", 1)
                    headers[header] = value.strip()
        except FileNotFoundError:
            pass

        self.send_response(http.HTTPStatus.OK)
        self.send_header("Content-type", "application/json")
        for header, value in headers.items():
            self.send_header(header, value)
        self.end_headers()
        self.wfile.write(response)

    def read_request_body(self):
        try:
            body_size = int(self.headers["Content-Length"])
        except (TypeError, ValueError):
            return None

        return self.rfile.read(body_size)


class CommentHTMLParser(html.parser.HTMLParser):
    """
    An HTML parser that extracts the comments (<!-- ... -->), tables, and
    paragraphs found in the markup passed to it.  Some inline tags are
    preserved so that Markdown contents turned into HTML can be inspected.
    """

    def __init__(self):
        super().__init__()
        self.comments = []
        self.tables = collections.OrderedDict()
        self.paragraphs = []
        self._capture_data = False
        self._list_to_append_to = None
        self._tag_html = None
        self._wrapper = None

    def handle_comment(self, data):
        self.comments.append(data)

    def handle_starttag(self, tag, attrs):
        if tag == "table":
            attributes = dict(attrs)
            assert "name" in attributes.keys()
            self.tables[attributes["name"]] = []
            self._list_to_append_to = self.tables[attributes["name"]]
        elif tag == "td":
            self._capture_data = True
            self._tag_html = []
            self._wrapper = None
        elif tag == "p":
            self._capture_data = True
            self._tag_html = []
            self._wrapper = None
            self._list_to_append_to = self.paragraphs
        elif tag == "em" or tag == "strong" or tag == "code":
            self._wrapper = tag

    def handle_data(self, data):
        if self._capture_data:
            if self._wrapper:
                self._tag_html.append(f"<{self._wrapper}>{data}</{self._wrapper}>")
            else:
                self._tag_html.append(data)

    def handle_endtag(self, tag):
        if tag == "table":
            self._list_to_append_to = None
        elif tag == "td":
            self._list_to_append_to.append("".join(self._tag_html))
            self._tag_html = None
            self._capture_data = False
        elif tag == "p":
            self._list_to_append_to.append("".join(self._tag_html))
            self._tag_html = None
            self._capture_data = False
            self._list_to_append_to = None
        elif tag == "em" or tag == "strong" or tag == "code":
            self._wrapper = None
