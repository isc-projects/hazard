# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

message("This is a *message*.")
warn("This is a **warning**.")
fail("This is a `failure`.")

message(danger.gitlab.mr.description.splitlines()[0])
warn(f"The target branch for this MR is '{danger.gitlab.mr.target_branch}'")
fail(danger.gitlab.mr.title)

message(danger.git.commits[0].message)
warn(danger.git.created_files[0])
fail(danger.git.modified_files[0])
