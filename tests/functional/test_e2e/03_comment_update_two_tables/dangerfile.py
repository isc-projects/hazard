# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

message("This is a different *message*.")
fail("This is a different `failure`.")

message(f"Merge request !{danger.gitlab.mr.iid}")
fail(f"Status: {danger.gitlab.mr.merge_status}")

message(danger.git.commits[1].message)
fail(danger.git.deleted_files[0])
