<!--
Copyright (C) Internet Systems Consortium, Inc. ("ISC")

SPDX-License-Identifier: MPL-2.0

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0.  If a copy of the MPL was not distributed with this
file, you can obtain one at https://mozilla.org/MPL/2.0/.
-->

# Hazard, a minimal danger-python replacement

## Overview

This project intends to be a drop-in replacement for [danger-python][1]
that does not rely on [Danger][2].  The rationale is to cut down on the
number of runtime dependencies necessary to evaluate `dangerfile.py`.

## Requirements

  - Python 3.8+
  - pygit2
  - python-gitlab

## Usage

This tool can currently only be used in a GitLab CI pipeline.

First, create a [project access token][3] with the necessary permissions
in the GitLab project you would like to use this tool with.

Then, make that token available to GitLab CI jobs by setting the
`DANGER_GITLAB_API_TOKEN` CI variable in your project to the value of
the access token.

Finally, add a job like this to your project's `.gitlab-ci.yml`:

    hazard:
      script:
        - pip install git+https://gitlab.isc.org/isc-projects/hazard.git
        - hazard

You can now proceed with creating `dangerfile.py` in your project's root
directory.

## Environment Variables

  - `DANGER_GITLAB_API_TOKEN` (required): GitLab CI token to use

  - `HAZARD_FETCH_DEPTH`: depth for fetching remote Git branches; the
    default value is 0, which fetches with full depth, but without
    unshallowing any previously-fetched shallow commits

[1]: https://github.com/danger/python/
[2]: https://github.com/danger/danger
[3]: https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html
