# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

"""
GitLab support
"""

from typing import Callable, Dict, Optional, Type, cast

import os

from gitlab.v4.objects.merge_requests import ProjectMergeRequest
from gitlab.v4.objects import ProjectMergeRequestNote

import gitlab


class HazardGitLabData:
    """
    A class collecting and exposing GitLab data associated with a merge request
    pointed to by environment variables (set by GitLab CI).
    """

    def __init__(
        self,
        _env: Optional[Dict[str, str]] = None,
        _gitlab: Optional[Type[gitlab.Gitlab]] = None,
    ) -> None:
        env = _env or os.environ
        self._server_url = env.get("CI_SERVER_URL")
        self._project_id = int(env.get("CI_MERGE_REQUEST_PROJECT_ID", "0"))
        self._merge_request_id = int(env.get("CI_MERGE_REQUEST_IID", "0"))
        self._access_token = env.get("DANGER_GITLAB_API_TOKEN", None)
        self._gitlab = _gitlab or gitlab.Gitlab
        # pylint: disable=invalid-name
        self.mr: Optional[ProjectMergeRequest] = None

    def fetch(self) -> None:
        """
        Connect to GitLab and fetch data associated with the merge request
        pointed to by the variables present in the environment.
        """
        gitlab_obj = self._gitlab(
            url=self._server_url, private_token=self._access_token
        )
        gitlab_project = gitlab_obj.projects.get(self._project_id)
        self.mr = gitlab_project.mergerequests.get(self._merge_request_id)

    def get_source_ref(
        self, _merge_request: Optional[ProjectMergeRequest] = None
    ) -> str:
        """
        Return the ref corresponding to the source branch of the provided merge
        request.
        """
        merge_request = _merge_request or self.mr
        assert merge_request

        return f"refs/merge-requests/{merge_request.iid}/head"

    def get_target_ref(
        self, _merge_request: Optional[ProjectMergeRequest] = None
    ) -> str:
        """
        Return the ref corresponding to the target branch of the provided merge
        request.
        """
        merge_request = _merge_request or self.mr
        assert merge_request

        return f"refs/heads/{merge_request.target_branch}"

    def _find_comment(
        self, _merge_request: Optional[ProjectMergeRequest] = None
    ) -> Optional[ProjectMergeRequestNote]:
        """
        Find and return the GitLab comment previously posted by the tool, or
        None if such a comment does not exist.
        """
        merge_request = _merge_request or self.mr
        assert merge_request

        for note in merge_request.notes.list(order_by="updated_at", iterator=True):
            if "DangerID: danger-id-Danger;" in note.body:
                return cast(ProjectMergeRequestNote, note)

        return None

    def sync_comment(
        self,
        body: str,
        _merge_request: Optional[ProjectMergeRequest] = None,
        _comment_finder: Optional[
            Callable[[], Optional[ProjectMergeRequestNote]]
        ] = None,
    ) -> None:
        """
        Update the comment posted on the merge request (i.e. create, update, or
        delete it, or do nothing at all, depending on whether the passed
        comment body is empty or not and whether a previously-posted comment is
        found on the merge request).
        """
        merge_request = _merge_request or self.mr
        assert merge_request

        old_comment = _comment_finder() if _comment_finder else self._find_comment()

        if body:
            if old_comment:
                merge_request.notes.update(old_comment.id, {"body": body})
            else:
                merge_request.notes.create({"body": body})
        elif old_comment:
            merge_request.notes.delete(old_comment.id)
