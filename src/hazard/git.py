# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

"""
Git support
"""

from typing import Any, Dict, List, Optional

import os
import pathlib

import pygit2


# pylint: disable=too-few-public-methods
class HazardGitData:
    """
    A class collecting and exposing Git data associated with a set of provided
    commits.
    """

    SOURCE_REF = "refs/heads/hazard/source"
    TARGET_REF = "refs/heads/hazard/target"

    def __init__(
        self, path: pathlib.Path, _env: Optional[Dict[str, str]] = None
    ) -> None:
        env = _env or os.environ
        repository_path = pygit2.discover_repository(str(path))
        self._repo = pygit2.Repository(repository_path)
        self._fetch_depth = int(env.get("HAZARD_FETCH_DEPTH", "0"))
        self.commits: Optional[List[HazardGitCommit]] = None
        self.created_files: Optional[List[str]] = None
        self.modified_files: Optional[List[str]] = None
        self.deleted_files: Optional[List[str]] = None

    class FetchException(Exception):
        """
        Raised when fetching any of the refs provided to fetch() fails.
        """

    def fetch(self, source_ref: str, target_ref: str) -> None:
        """
        Fetch the source and target refs from the "origin" remote and gather
        all the data associated with the differences between these two refs.
        """
        self._fetch_refs(source_ref, target_ref)
        self._prepare_commits()
        self._prepare_files()

    def _fetch_refs(self, source_ref: str, target_ref: str) -> None:
        self._fetch_ref_and_ensure_it_exists(source_ref, self.SOURCE_REF)
        self._fetch_ref_and_ensure_it_exists(target_ref, self.TARGET_REF)

    def _fetch_ref_and_ensure_it_exists(self, remote_ref: str, local_ref: str) -> None:
        remote = self._repo.remotes["origin"]
        remote.fetch([f"{remote_ref}:{local_ref}"], depth=self._fetch_depth)
        if local_ref not in self._repo.references:
            raise self.FetchException(f"{remote_ref} not found on the 'origin' remote")

    def _prepare_commits(self) -> None:
        walker = self._repo.walk(None)
        walker.push(self._repo.references[self.SOURCE_REF].target)
        walker.hide(self._repo.references[self.TARGET_REF].target)

        self.commits = []
        for commit in walker:
            self.commits.append(HazardGitCommit(commit))

    def _prepare_files(self) -> None:
        source = self._repo.references[self.SOURCE_REF].target
        target = self._repo.references[self.TARGET_REF].target
        merge_base = self._repo.merge_base(source, target)
        diff = self._repo.diff(merge_base, source)

        self.created_files = []
        self.modified_files = []
        self.deleted_files = []
        for change in diff:
            if change.delta.old_file.mode == 0:
                self.created_files.append(change.delta.new_file.path)
            elif change.delta.new_file.mode == 0:
                self.deleted_files.append(change.delta.old_file.path)
            else:
                self.modified_files.append(change.delta.new_file.path)

    @property
    def head(self) -> str:
        """
        The SHA-1 hash of the current head of the source branch (i.e. the
        commit hash that the tool is run against).
        """
        assert self.commits
        return str(self.commits[0].id)


class HazardGitCommit:
    """
    A helper class to map attributes missing from pygit2.Commit but set by
    Danger.
    """

    def __init__(self, commit: pygit2.Commit) -> None:
        self._commit = commit

    def __getattr__(self, attr: str) -> Any:
        if attr == "sha":
            return str(self._commit.id)
        return getattr(self._commit, attr)
