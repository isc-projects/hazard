# Copyright (C) Internet Systems Consortium, Inc. ("ISC")
#
# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0.  If a copy of the MPL was not distributed with this
# file, you can obtain one at https://mozilla.org/MPL/2.0/.

"""
GitLab comment generator
"""

from typing import List

import markdown


BULLETS = {
    "Messages": ":book:",
    "Warnings": ":warning:",
    "Failures": ":no_entry_sign:",
}


class HazardComment:
    """
    A class generating GitLab comment contents.
    """

    def __init__(self) -> None:
        self._markup: str = ""

    def generate(self) -> str:
        """
        Return generated comment contents.
        """
        return self._markup

    def add_comment(self, text: str) -> None:
        """
        Add a HTML comment to the generated markup.
        """
        self._markup += f"<!-- {text} -->\n"

    def add_table(self, title: str, entries: List[str]) -> None:
        """
        Add a HTML table with the provided title and entries to the generated
        markup.
        """
        self._markup += f'<table name="{title.lower()}">\n'
        self._markup += "\t<thead>\n"
        self._markup += "\t\t<tr>\n"
        self._markup += "\t\t\t<th></th>\n"
        self._markup += f'\t\t\t<th width="100%">{title}</th>\n'
        self._markup += "\t\t</tr>\n"
        self._markup += "\t</thead>\n"
        self._markup += "\t<tbody>\n"
        for entry in entries:
            markdown_entry = markdown.markdown(entry)
            markdown_entry = markdown_entry.replace("<p>", "")
            markdown_entry = markdown_entry.replace("</p>", "")
            self._markup += "\t\t<tr>\n"
            self._markup += f"\t\t\t<td>{BULLETS.get(title, '')}</td>\n"
            self._markup += f"\t\t\t<td>{markdown_entry}</td>\n"
            self._markup += "\t\t</tr>\n"
        self._markup += "\t</tbody>\n"
        self._markup += "</table>\n"
        self._markup += "\n"

    def add_footer(self, revision: str) -> None:
        """
        Add a footer (including the provided code revision identifier) to the
        generated markup.
        """
        link = '<a href="https://gitlab.isc.org/isc-projects/hazard">Hazard</a>'
        self._markup += f"<p>Generated by {link} against {revision}</p>"
